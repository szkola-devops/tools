# Tools image

This repository contains the Dockerfile with tools used in SzkolaDevOps

## How to build locally

    docker build -t tools:TAG_NAME .

## How to use image from registry

    docker run --rm -it registry.gitlab.com/szkola-devops/tools:v2 /bin/bash