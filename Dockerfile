FROM ubuntu:18.04

ENV TERRAFORM_VERSION="0.12.7"

COPY scripts /scripts

RUN apt-get update && \
    apt-get install -y wget unzip software-properties-common && \
    /scripts/ansible.sh && \
    /scripts/terraform.sh